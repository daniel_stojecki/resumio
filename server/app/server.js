import express from 'express';
import cors from 'cors'
import dotenv from 'dotenv';

dotenv.config()
const port = 8000;
const server = express();

const corsOptions = {
    origin: function (origin, callback) {
        if (origin == process.env.ALLOW_CORS) {
            callback(null, true);
        } else {
            console.log(origin, process.env.ALLOW_CORS);
            callback(new Error('Not allowed by CORS'));
        }
    },
};

server.use(cors(corsOptions));
server.use(express.json());
server.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

export default server 