import express from 'express';
import { assistant } from "../services/open-ai.js";
import { wait } from '../helpers/helpers.js'

const router = express.Router();

router.post('/message', async (req, res) => {
    try {
        const message = req.body.message;
        let threadId = req.body.threadId;

        if (!message) {
            return res.status(400).send({ error: 'Message is required' });
        } else if (message.length > 500) {
            return res.status(400).send({ error: 'Message must contain less then 500 characters.' });
        }

        if (!threadId) {
            threadId = await assistant.createThread()
        }

        await assistant.openai.beta.threads.messages.create(threadId, {
            role: 'user',
            content: message,
        });

        const runRq = await assistant.openai.beta.threads.runs.create(threadId, { assistant_id: assistant.myAssistant.id });

        let runStatus = await assistant.openai.beta.threads.runs.retrieve(threadId, runRq.id);

        while (runStatus.status !== "completed") {
            await wait(200);
            runStatus = await assistant.openai.beta.threads.runs.retrieve(threadId, runRq.id);
        }

        const messages = await assistant.openai.beta.threads.messages.list(threadId);
        const response = {
            conversation: messages.data.map(message => { return message.content[0].text.value }),
            message: messages.data[0].content[0].text.value,
            threadId,
        }

        res.send(response);
    } catch (error) {
        console.error('Error processing your request:', error);
        res.status(500).send({ error: 'Error processing your request' });
    }
});

router.get('/', async (req, res) => {
    res.send('Hello World!');
})

router.get('/test', async (req, res) => {
    res.send('super');
})

export default router;