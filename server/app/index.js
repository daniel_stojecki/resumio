import router from "./routes/open-ai.js";
import server from "./server.js";
import { assistant } from "./services/open-ai.js";

server.use('/api', router);