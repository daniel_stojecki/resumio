import OpenAI from "openai";
import dotenv from 'dotenv';

dotenv.config()
class AssistantOpenAI {
    constructor() {
        this.myAssistant = '';
        this.openai = new OpenAI({
            apiKey: process.env.OPENAI_API_KEY,
        });

        this.init()
    }

    async init() {
        this.myAssistant = await this.openai.beta.assistants.retrieve(
            process.env.OPENAI_ASSISTANT_KEY
        );
    }

    async createThread() {
        const thread = await this.openai.beta.threads.create()

        return thread.id
    }
}

const assistant = new AssistantOpenAI()

export { assistant, AssistantOpenAI }